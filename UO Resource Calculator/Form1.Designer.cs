﻿namespace UO_Resource_Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.lbResult = new System.Windows.Forms.Label();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.tbTotalMacroTime = new System.Windows.Forms.TextBox();
            this.lbTotalMacroTIme = new System.Windows.Forms.Label();
            this.tbTotalResourcesNeeded = new System.Windows.Forms.TextBox();
            this.lbTotalResourcesNeeded = new System.Windows.Forms.Label();
            this.tbTimeBetweenTicks = new System.Windows.Forms.TextBox();
            this.lbTimeBetweenTicks = new System.Windows.Forms.Label();
            this.lbResourcesUsedPerTick = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rbTotalMacroTime = new System.Windows.Forms.RadioButton();
            this.rbAmountOfResourcesUsed = new System.Windows.Forms.RadioButton();
            this.tbResourcesUsedPerTick = new System.Windows.Forms.TextBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.lbResult);
            this.groupBox1.Controls.Add(this.tbResult);
            this.groupBox1.Controls.Add(this.btnCalculate);
            this.groupBox1.Controls.Add(this.tbTotalMacroTime);
            this.groupBox1.Controls.Add(this.lbTotalMacroTIme);
            this.groupBox1.Controls.Add(this.tbTotalResourcesNeeded);
            this.groupBox1.Controls.Add(this.lbTotalResourcesNeeded);
            this.groupBox1.Controls.Add(this.tbTimeBetweenTicks);
            this.groupBox1.Controls.Add(this.lbTimeBetweenTicks);
            this.groupBox1.Controls.Add(this.lbResourcesUsedPerTick);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rbTotalMacroTime);
            this.groupBox1.Controls.Add(this.rbAmountOfResourcesUsed);
            this.groupBox1.Controls.Add(this.tbResourcesUsedPerTick);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(476, 327);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Resource Calculator";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(200, 212);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(120, 33);
            this.btnClear.TabIndex = 15;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.BtnClear_Click);
            // 
            // lbResult
            // 
            this.lbResult.AutoSize = true;
            this.lbResult.Location = new System.Drawing.Point(200, 155);
            this.lbResult.Name = "lbResult";
            this.lbResult.Size = new System.Drawing.Size(177, 13);
            this.lbResult.TabIndex = 14;
            this.lbResult.Text = "Total Amount of Resources Needed";
            // 
            // tbResult
            // 
            this.tbResult.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbResult.Location = new System.Drawing.Point(200, 174);
            this.tbResult.Name = "tbResult";
            this.tbResult.ReadOnly = true;
            this.tbResult.Size = new System.Drawing.Size(256, 20);
            this.tbResult.TabIndex = 13;
            // 
            // btnCalculate
            // 
            this.btnCalculate.BackColor = System.Drawing.SystemColors.Highlight;
            this.btnCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalculate.ForeColor = System.Drawing.SystemColors.Window;
            this.btnCalculate.Location = new System.Drawing.Point(335, 212);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(121, 33);
            this.btnCalculate.TabIndex = 12;
            this.btnCalculate.Text = "Calculate";
            this.btnCalculate.UseVisualStyleBackColor = false;
            this.btnCalculate.Click += new System.EventHandler(this.BtnCalculate_Click);
            // 
            // tbTotalMacroTime
            // 
            this.tbTotalMacroTime.Location = new System.Drawing.Point(28, 290);
            this.tbTotalMacroTime.Name = "tbTotalMacroTime";
            this.tbTotalMacroTime.Size = new System.Drawing.Size(121, 20);
            this.tbTotalMacroTime.TabIndex = 11;
            this.tbTotalMacroTime.Validating += new System.ComponentModel.CancelEventHandler(this.TbTotalMacroTime_Validating);
            // 
            // lbTotalMacroTIme
            // 
            this.lbTotalMacroTIme.AutoSize = true;
            this.lbTotalMacroTIme.Location = new System.Drawing.Point(28, 274);
            this.lbTotalMacroTIme.Name = "lbTotalMacroTIme";
            this.lbTotalMacroTIme.Size = new System.Drawing.Size(121, 13);
            this.lbTotalMacroTIme.TabIndex = 10;
            this.lbTotalMacroTIme.Text = "Total Macro Time (Mins)";
            // 
            // tbTotalResourcesNeeded
            // 
            this.tbTotalResourcesNeeded.Location = new System.Drawing.Point(28, 238);
            this.tbTotalResourcesNeeded.Name = "tbTotalResourcesNeeded";
            this.tbTotalResourcesNeeded.Size = new System.Drawing.Size(121, 20);
            this.tbTotalResourcesNeeded.TabIndex = 9;
            this.tbTotalResourcesNeeded.Visible = false;
            this.tbTotalResourcesNeeded.Validating += new System.ComponentModel.CancelEventHandler(this.TbTotalResourcesNeeded_Validating);
            // 
            // lbTotalResourcesNeeded
            // 
            this.lbTotalResourcesNeeded.AutoSize = true;
            this.lbTotalResourcesNeeded.Location = new System.Drawing.Point(28, 222);
            this.lbTotalResourcesNeeded.Name = "lbTotalResourcesNeeded";
            this.lbTotalResourcesNeeded.Size = new System.Drawing.Size(154, 13);
            this.lbTotalResourcesNeeded.TabIndex = 8;
            this.lbTotalResourcesNeeded.Text = "Total Resources Needed/Avail";
            this.lbTotalResourcesNeeded.Visible = false;
            // 
            // tbTimeBetweenTicks
            // 
            this.tbTimeBetweenTicks.Location = new System.Drawing.Point(28, 190);
            this.tbTimeBetweenTicks.Name = "tbTimeBetweenTicks";
            this.tbTimeBetweenTicks.Size = new System.Drawing.Size(121, 20);
            this.tbTimeBetweenTicks.TabIndex = 7;
            this.tbTimeBetweenTicks.Validating += new System.ComponentModel.CancelEventHandler(this.TbTimeBetweenTicks_Validating);
            // 
            // lbTimeBetweenTicks
            // 
            this.lbTimeBetweenTicks.AutoSize = true;
            this.lbTimeBetweenTicks.Location = new System.Drawing.Point(28, 174);
            this.lbTimeBetweenTicks.Name = "lbTimeBetweenTicks";
            this.lbTimeBetweenTicks.Size = new System.Drawing.Size(155, 13);
            this.lbTimeBetweenTicks.TabIndex = 6;
            this.lbTimeBetweenTicks.Text = "Time Between Ticks (Seconds)\r\n";
            // 
            // lbResourcesUsedPerTick
            // 
            this.lbResourcesUsedPerTick.AutoSize = true;
            this.lbResourcesUsedPerTick.Location = new System.Drawing.Point(28, 120);
            this.lbResourcesUsedPerTick.Name = "lbResourcesUsedPerTick";
            this.lbResourcesUsedPerTick.Size = new System.Drawing.Size(180, 13);
            this.lbResourcesUsedPerTick.TabIndex = 5;
            this.lbResourcesUsedPerTick.Text = "Resources Used Per Tick (Seconds)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Calculate Based On:";
            // 
            // rbTotalMacroTime
            // 
            this.rbTotalMacroTime.AutoSize = true;
            this.rbTotalMacroTime.Location = new System.Drawing.Point(28, 79);
            this.rbTotalMacroTime.Name = "rbTotalMacroTime";
            this.rbTotalMacroTime.Size = new System.Drawing.Size(139, 17);
            this.rbTotalMacroTime.TabIndex = 3;
            this.rbTotalMacroTime.TabStop = true;
            this.rbTotalMacroTime.Text = "Total Macro Time (Mins)";
            this.rbTotalMacroTime.UseVisualStyleBackColor = true;
            this.rbTotalMacroTime.CheckedChanged += new System.EventHandler(this.RbTotalMacroTime_CheckedChanged);
            // 
            // rbAmountOfResourcesUsed
            // 
            this.rbAmountOfResourcesUsed.AutoSize = true;
            this.rbAmountOfResourcesUsed.Checked = true;
            this.rbAmountOfResourcesUsed.Location = new System.Drawing.Point(28, 62);
            this.rbAmountOfResourcesUsed.Name = "rbAmountOfResourcesUsed";
            this.rbAmountOfResourcesUsed.Size = new System.Drawing.Size(168, 17);
            this.rbAmountOfResourcesUsed.TabIndex = 1;
            this.rbAmountOfResourcesUsed.TabStop = true;
            this.rbAmountOfResourcesUsed.Text = "Amount of Resources Needed";
            this.rbAmountOfResourcesUsed.UseVisualStyleBackColor = true;
            this.rbAmountOfResourcesUsed.CheckedChanged += new System.EventHandler(this.RbAmountOfResourcesUsed_CheckedChanged);
            // 
            // tbResourcesUsedPerTick
            // 
            this.tbResourcesUsedPerTick.Location = new System.Drawing.Point(28, 139);
            this.tbResourcesUsedPerTick.Name = "tbResourcesUsedPerTick";
            this.tbResourcesUsedPerTick.Size = new System.Drawing.Size(121, 20);
            this.tbResourcesUsedPerTick.TabIndex = 0;
            this.tbResourcesUsedPerTick.Validating += new System.ComponentModel.CancelEventHandler(this.TbResourcesUsedPerTick_Validating);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(500, 351);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "UO Resource Calculator";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbResourcesUsedPerTick;
        private System.Windows.Forms.RadioButton rbAmountOfResourcesUsed;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbTotalMacroTime;
        private System.Windows.Forms.Button btnCalculate;
        private System.Windows.Forms.TextBox tbTotalMacroTime;
        private System.Windows.Forms.Label lbTotalMacroTIme;
        private System.Windows.Forms.TextBox tbTotalResourcesNeeded;
        private System.Windows.Forms.Label lbTotalResourcesNeeded;
        private System.Windows.Forms.Label lbResourcesUsedPerTick;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lbResult;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.TextBox tbTimeBetweenTicks;
        private System.Windows.Forms.Label lbTimeBetweenTicks;
        private System.Windows.Forms.ErrorProvider errorProvider1;
    }
}

