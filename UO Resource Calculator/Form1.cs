﻿using System;
using System.Windows.Forms;

namespace UO_Resource_Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void RbAmountOfResourcesUsed_CheckedChanged(object sender, EventArgs e)
        {
            lbTotalResourcesNeeded.Visible = false;
            tbTotalResourcesNeeded.Visible = false;

            lbResourcesUsedPerTick.Visible = true;
            tbResourcesUsedPerTick.Visible = true;

            lbTimeBetweenTicks.Visible = true;
            tbTimeBetweenTicks.Visible = true;

            lbTotalMacroTIme.Visible = true;
            tbTotalMacroTime.Visible = true;

            tbResult.Text = "";
            lbResult.Text = "Total Amount of Resources Needed";
        }

        private void RbTotalMacroTime_CheckedChanged(object sender, EventArgs e)
        {
            lbTotalResourcesNeeded.Visible = true;
            tbTotalResourcesNeeded.Visible = true;

            lbResourcesUsedPerTick.Visible = true;
            tbResourcesUsedPerTick.Visible = true;

            lbTimeBetweenTicks.Visible = true;
            tbTimeBetweenTicks.Visible = true;

            lbTotalMacroTIme.Visible = false;
            tbTotalMacroTime.Visible = false;

            tbResult.Text = "";
            lbResult.Text = "Total Macro Time Needed";
        }

        private void BtnCalculate_Click(object sender, EventArgs e)
        {
            bool validResourcesUsed = validateResourcesUsedText();
            bool validTimeBetweenTicks = validateTimeBetweenTicksText();
            bool validTotalMacroTime = validateTotalMacroTimeText();
            bool validTotalResourcesNeeded = validateTotalReourcesNeededText();

            if(validResourcesUsed && validTimeBetweenTicks && validTotalMacroTime)
            {
                calculateResult();
            } else if (validResourcesUsed && validTimeBetweenTicks && validTotalResourcesNeeded)
            {
                calculateResult();
            }
            
        }

        private void BtnClear_Click(object sender, EventArgs e)
        {
            rbAmountOfResourcesUsed.Checked = false;
            rbTotalMacroTime.Checked = false;

            tbResourcesUsedPerTick.Text = null;
            tbTimeBetweenTicks.Text = null;
            tbTotalResourcesNeeded.Text = null;
            tbTotalMacroTime.Text = null;

            tbResult.Text = null;

            lbResult.Text = "Result";
        }

        public void calculateResult()
        {
            if (rbAmountOfResourcesUsed.Checked)
            {
                int resourcesUsedPerTick = int.Parse(tbResourcesUsedPerTick.Text);
                int timeBetweenTicks = int.Parse(tbTimeBetweenTicks.Text);
                int totalMacroTime = int.Parse(tbTotalMacroTime.Text);

                int totalMacroTimeInSeconds = totalMacroTime * 60;

                int totalResourcesNeeded = (totalMacroTimeInSeconds / timeBetweenTicks) * resourcesUsedPerTick;

                tbResult.Text = totalResourcesNeeded.ToString();
            }
            else if (rbTotalMacroTime.Checked)
            {
                int resourcesUsedPerTick = int.Parse(tbResourcesUsedPerTick.Text);
                int timeBetweenTicks = int.Parse(tbTimeBetweenTicks.Text);
                int totalResourcesNeeded = int.Parse(tbTotalResourcesNeeded.Text);

                decimal result = (totalResourcesNeeded / resourcesUsedPerTick) * timeBetweenTicks;

                if (result <= 60)
                {
                    tbResult.Text = result.ToString() + " seconds";
                }
                else
                {
                    int totalHours = (int)result / 60 / 60;
                    int totalMinutes = (int)(result / 60) % 60;
                    int totalSeconds = (int)result % 60;

                    string hours = totalHours == 1 ? totalHours.ToString() + " hour" : totalHours.ToString() + " hours";
                    string minutes = totalMinutes == 1 ? totalMinutes.ToString() + " minute" : totalMinutes.ToString() + " minutes";
                    string seconds = totalSeconds == 1 ? totalSeconds.ToString() + " second" : totalSeconds.ToString() + " seconds";

                    if (totalHours > 0)
                    {

                        tbResult.Text = hours + ", " + minutes + ", and " + seconds;
                    }
                    else
                    {
                        tbResult.Text = minutes + " and " + seconds;
                    }
                }
            }
            else
            {
                MessageBox.Show("Please select a calculation to perform.");
            }
        }

        public bool validateResourcesUsedText()
        {
            bool bStatus = true;
            int value;
            if (tbResourcesUsedPerTick.Text == "" || !int.TryParse(tbResourcesUsedPerTick.Text, out value))
            {
                errorProvider1.SetError(tbResourcesUsedPerTick, "Please enter a valid number.");
                bStatus = false;
            }
            else
                errorProvider1.SetError(tbResourcesUsedPerTick, "");
            return bStatus;
        }

        public bool validateTimeBetweenTicksText()
        {
            bool bStatus = true;
            int value;
            if (tbTimeBetweenTicks.Text == "" || !int.TryParse(tbTimeBetweenTicks.Text, out value))
            {
                errorProvider1.SetError(tbTimeBetweenTicks, "Please enter a valid number.");
                bStatus = false;
            }
            else
                errorProvider1.SetError(tbTimeBetweenTicks, "");
            return bStatus;
        }

        public bool validateTotalReourcesNeededText()
        {
            bool bStatus = true;
            int value;
            if (tbTotalResourcesNeeded.Text == "" || !int.TryParse(tbTotalResourcesNeeded.Text, out value))
            {
                errorProvider1.SetError(tbTotalResourcesNeeded, "Please enter a valid number.");
                bStatus = false;
            }
            else
                errorProvider1.SetError(tbTotalResourcesNeeded, "");
            return bStatus;
        }

        public bool validateTotalMacroTimeText()
        {
            bool bStatus = true;
            int value;
            if (tbTotalMacroTime.Text == "" || !int.TryParse(tbTotalMacroTime.Text, out value))
            {
                errorProvider1.SetError(tbTotalMacroTime, "Please enter a valid number.");
                bStatus = false;
            }
            else
                errorProvider1.SetError(tbTotalMacroTime, "");
            return bStatus;
        }

        private void TbResourcesUsedPerTick_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            validateResourcesUsedText();
        }

        private void TbTimeBetweenTicks_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            validateTimeBetweenTicksText();
        }

        private void TbTotalResourcesNeeded_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            validateTotalReourcesNeededText();
        }

        private void TbTotalMacroTime_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            validateTotalMacroTimeText();
        }
    }
}
